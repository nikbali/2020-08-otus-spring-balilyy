package ru.otus.service

import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import ru.otus.exception.ApiException
import ru.otus.model.Author
import ru.otus.model.Book
import ru.otus.model.Genre
import java.util.stream.Collectors

@DisplayName("В BookServiceTest книги ")
@SpringBootTest
internal class BookServiceImplTest {

    @Autowired
    private val bookService: BookService? = null

    @Autowired
    private val authorService: AuthorService? = null

    @Autowired
    private val genreService: GenreService? = null


    @DisplayName(" создаются в БД и работает поиск по Id")
    @Test
    fun createBookAndFindById() {
        val genre1 = genreService!!.create("Adventure")
        val genre2 = genreService.create("Fantasy")
        val author1 = authorService!!.create("Joanne", "Rowling")

        //Жанры: Fantasy, Adventure
        //Авторы:  Joanne Rowling
        val book = bookService!!.create(
            "Гарри Поттер и тайная комната",
            setOf(genre1?.id ?: throw RuntimeException(), genre2?.id ?: throw RuntimeException()),
            setOf(author1?.id ?: throw RuntimeException())
        )

        MatcherAssert.assertThat(book, Matchers.notNullValue())
        MatcherAssert.assertThat(book!!.title, Matchers.`is`("Гарри Поттер и тайная комната"))
        MatcherAssert.assertThat<Set<Author>>(book.authors, Matchers.notNullValue())
        MatcherAssert.assertThat<Set<Genre>>(book.genres, Matchers.notNullValue())
        MatcherAssert.assertThat(
            book.authors!!.stream()
                .map { author: Author -> author.firstName + " " + author.lastName }
                .collect(Collectors.toSet()),
            Matchers.contains("Joanne Rowling"))
        MatcherAssert.assertThat(
            book.genres!!.stream().map(Genre::name).collect(Collectors.toSet()),
            Matchers.hasItems("Fantasy", "Adventure")
        )
    }

    @DisplayName(" создает, а потом обновляет у книги название, авторов и жанр")
    @Test
    fun createThenUpdateBook() {
        val genre1 = genreService!!.create("Adventure")
        val genre2 = genreService.create("Fantasy")
        val author1 = authorService!!.create("Stephen", "King")
        val author2 = authorService.create("Joanne", "Rowling")
        val author3 = authorService.create("Fyodor", "Dostoevsky")

        //Жанры: Adventure
        //Авторы:  Stephen King, Joanne Rowling
        val book = bookService!!.create(
            "Жизнь",
            setOf(genre1!!.id ?: throw RuntimeException()),
            setOf(author1!!.id ?: throw RuntimeException(), author2!!.id ?: throw RuntimeException())
        )

        //Жанры: Fantasy
        //Авторы:  Fyodor Dostoevsky, Joanne Rowling
        bookService.update(
            book!!.id!!,
            "Другая жизнь",
            setOf(genre2!!.id ?: throw RuntimeException()),
            setOf(author3!!.id ?: throw RuntimeException(), author2.id ?: throw RuntimeException())
        )
        val persistBook = bookService.getById(book.id!!)
        MatcherAssert.assertThat(persistBook, Matchers.notNullValue())
        MatcherAssert.assertThat(persistBook!!.title, Matchers.`is`("Другая жизнь"))
        MatcherAssert.assertThat<Set<Author>>(persistBook.authors, Matchers.notNullValue())
        MatcherAssert.assertThat<Set<Genre>>(persistBook.genres, Matchers.notNullValue())
        MatcherAssert.assertThat(
            persistBook.authors!!.stream()
                .map { author: Author -> author.firstName + " " + author.lastName }
                .collect(Collectors.toSet()),
            Matchers.hasItems("Joanne Rowling", "Fyodor Dostoevsky"))
        MatcherAssert.assertThat(
            persistBook.genres!!.stream().map(Genre::name).collect(Collectors.toSet()),
            Matchers.hasItems("Fantasy")
        )
    }

    @DisplayName(" создает, а потом удаляет книгу из БД и в конце проверяет, что книга удалена")
    @Test
    fun createThenDeleteBook() {
        val book = bookService!!.create("Боль", java.util.Set.of(1), java.util.Set.of(1, 3))
        val id = book!!.id!!
        bookService.deleteById(id)
        val exception = Assertions.assertThrows(ApiException::class.java) { bookService.getById(id) }
        MatcherAssert.assertThat(exception.message, Matchers.`is`(String.format("Book not found by id = %s", id)))
    }

    @DisplayName(" создает 3 книги и проверяет выгрузку списка всех книг")
    @Test
    fun createThreeBooksAndCheckGetAll() {
        bookService!!.create("Боль 1", java.util.Set.of(1), java.util.Set.of(1, 3))
        bookService.create("Боль 2", java.util.Set.of(1), java.util.Set.of(1, 3))
        bookService.create("Боль 3", java.util.Set.of(1), java.util.Set.of(1, 3))
        val books = bookService.getAll()
        MatcherAssert.assertThat(books, Matchers.notNullValue())
        MatcherAssert.assertThat(books!!.size, Matchers.`is`(3))
        MatcherAssert.assertThat(
            books.stream()
                .map(Book::title)
                .collect(Collectors.toList()),
            Matchers.contains("Боль 1", "Боль 2", "Боль 3")
        )
    }
}