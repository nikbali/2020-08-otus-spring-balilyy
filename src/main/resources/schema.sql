drop table if exists genres;
drop table if exists authors;
drop table if exists books;
drop table if exists books_to_authors;

create table genres
(
    id   int auto_increment primary key,
    name varchar(255) not null unique
);

create table authors
(
    id         int auto_increment primary key,
    first_name varchar(255) not null,
    last_name  varchar(255) not null
);
create unique index first_name_and_last_name_idx on authors(first_name, last_name);

create table books
(
    id    int auto_increment primary key,
    title varchar(255) not null
);

create table comments
(
    id    int auto_increment primary key,
    text  varchar(10000),
    book_id   int not null,
    constraint fk_comments_book_id foreign key (book_id) references books (id)
);

create table books_to_authors
(
    id         int auto_increment primary key,
    book_id   int not null,
    author_id int not null,
    constraint fk_books_to_authors_book_id foreign key (book_id) references books (id),
    constraint fk_books_to_authors_author_id foreign key (author_id) references authors (id)
);

create table books_to_genres
(
    id        int auto_increment primary key,
    book_id  int not null,
    genre_id int not null,
    constraint fk_books_to_genres_book_id foreign key (book_id) references books (id),
    constraint fk_books_to_genres_genre_id foreign key (genre_id) references genres (id)
);

