insert into genres (id, `name`)
values (1, 'Thriller');
insert into genres (id, `name`)
values (2, 'Fantasy');
insert into genres (id, `name`)
values (3, 'Adventure');
insert into genres (id, `name`)
values (4, 'Drama');


insert into authors (id, first_name, last_name)
values (1, 'Stephen', 'King');
insert into authors (id, first_name, last_name)
values (2, 'Fyodor', 'Dostoevsky');
insert into authors (id, first_name, last_name)
values (3, 'Joanne', 'Rowling');
insert into authors (id, first_name, last_name)
values (4, 'Jack', 'London');


-- The Idiot - F. Dostoevsky
insert into books (id, title)
values (1, 'The Idiot');

insert into books_to_genres (id, book_id, genre_id)
values (1, 1, 4);

insert into books_to_authors (id, book_id, author_id)
values (1, 1, 2);


-- Martin Eden - J. London
insert into books (id, title)
values (2, 'Martin Eden');

insert into books_to_genres (id, book_id, genre_id)
values (2, 2, 3);
insert into books_to_genres (id, book_id, genre_id)
values (3, 2, 4);

insert into books_to_authors (id, book_id, author_id)
values (2, 2, 4);