package ru.otus.service

import ru.otus.model.Genre

interface GenreService {
    fun create(title: String): Genre?
    fun update(id: Int, title: String)
    fun deleteById(id: Int)
    fun getAll(): List<Genre>?
    fun getById(id: Int): Genre?
}