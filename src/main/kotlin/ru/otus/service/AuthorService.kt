package ru.otus.service

import ru.otus.model.Author

interface AuthorService {
    fun create(firstName: String, lastName: String): Author?
    fun update(id: Int, firstName: String, lastName: String)
    fun deleteById(id: Int)
    fun getAll(): List<Author>?
    fun getById(id: Int): Author?
}