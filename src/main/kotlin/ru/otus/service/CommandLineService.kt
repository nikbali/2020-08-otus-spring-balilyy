package ru.otus.service

import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellOption
import ru.otus.model.Author
import ru.otus.model.Book
import ru.otus.model.Comment
import ru.otus.model.Genre

@ShellComponent
class CommandLineService(
    private val genreService: GenreService,
    private val bookService: BookService,
    private val authorService: AuthorService,
    private val commentService: CommentService
) {

    /**
     * Создает книгу в библиотеке
     *
     * @param title     название книги
     * @param authorIds id авторов через запятую
     * @param genreIds  id жанров через запятую
     */
    @ShellMethod(key = ["create-book"], value = "Create new book")
    fun createBook(
        @ShellOption(value = ["-t", "--title"], help = "Book`s title") title: String,
        @ShellOption(
            value = ["-a", "--authors"],
            help = "Ids authors",
            defaultValue = ShellOption.NULL
        ) authorIds: String?,
        @ShellOption(value = ["-g", "--genres"], help = "Ids genres", defaultValue = ShellOption.NULL) genreIds: String?
    ) {

        bookService.create(
            title,
            parseIdFromString(genreIds),
            parseIdFromString(authorIds)
        )
    }

    /**
     * Обновляет у книги название, авторов и жанры
     *
     * @param id        идентификатор книги в бд
     * @param title     название книги
     * @param authorIds id авторов через запятую
     * @param genreIds  id жанров через запятую
     */
    @ShellMethod(key = ["update-book"], value = "Update Book")
    fun updateBook(
        @ShellOption(value = ["--id"], help = "Book Id") id: Int,
        @ShellOption(value = ["-t", "--title"], help = "Book`s title") title: String?,
        @ShellOption(
            value = ["-a", "--authors"],
            help = "Ids authors",
            defaultValue = ShellOption.NULL
        ) authorIds: String?,
        @ShellOption(value = ["-g", "--genres"], help = "Ids genres", defaultValue = ShellOption.NULL) genreIds: String?
    ) {
        bookService.update(id, title, parseIdFromString(genreIds), parseIdFromString(authorIds))
    }

    /**
     * Поиск книги по id
     *
     * @param id идентификатор книги в бд
     * @return книга как строка
     */
    @ShellMethod(key = ["get-book"], value = "Get Book by Id")
    fun getBookById(@ShellOption(value = ["--id"], help = "Book Id") id: Int): Book? {
        return bookService.getById(id)
    }

    /**
     * Возвращает все книги из бд
     */
    @ShellMethod(key = ["get-all-books"], value = "Get all Books")
    fun allBooks(): List<Book?>? {
        return bookService.getAll()
    }


    /**
     * Удаляет книгу по id
     *
     * @param id идентификатор книги в бд
     */
    @ShellMethod(key = ["delete-book"], value = "Delete Book by Id")
    fun deleteBookById(@ShellOption(value = ["--id"], help = "Book Id") id: Int) {
        bookService.deleteById(id)
    }

    /**
     * Добавляет жанр
     *
     * @param title название жанра
     */
    @ShellMethod(key = ["create-genre"], value = "Create genre")
    fun createGenre(@ShellOption(value = ["-t", "--title"], help = "Genre`s title") title: String) {
        genreService.create(title)
    }

    /**
     * Возвращает все доступные жанры
     */
    @ShellMethod("Get all genres")
    fun allGenres(): List<Genre?>? {
        return genreService.getAll()
    }


    /**
     * Добавляет автора в библиотеку
     *
     * @param firstName имя автора
     * @param lastName  фамилия автора
     */
    @ShellMethod(key = ["create-author"], value = "Create new author")
    fun createAuthor(
        @ShellOption(value = ["-f", "--firstName"], help = "First name") firstName: String,
        @ShellOption(value = ["-l", "--lastName"], help = "Last name") lastName: String
    ) {
        authorService.create(firstName, lastName)
    }

    /**
     * Возвращает всех авторов
     */
    @ShellMethod("Get all authors")
    fun allAuthors(): List<Author?>? {
        return authorService.getAll()
    }


    /**
     * Добавляет комментарий к книге
     *
     * @param bookId  id книги
     * @param comment комментарий
     */
    @ShellMethod(key = ["add-comment"], value = "Add comment to book")
    fun addComment(
        @ShellOption(value = ["--bookId"], help = "Book Id") bookId: Int,
        @ShellOption(value = ["-c", "--commnet"], help = "Comment") comment: String
    ) {
        commentService.add(bookId, comment)
    }

    /**
     * Редактирует комментарий
     *
     * @param id      id коммента
     * @param comment комментарий
     */
    @ShellMethod(key = ["edit-comment"], value = "Edit comment")
    fun editComment(
        @ShellOption(value = ["--id"]) id: Int,
        @ShellOption(value = ["-c", "--commnet"], help = "Comment") comment: String
    ) {
        commentService.update(id, comment)
    }

    /**
     * Удаляет комментарий
     *
     * @param id id коммента
     */
    @ShellMethod(key = ["delete-comment"], value = "Delete comment")
    fun deleteComment(@ShellOption(value = ["--id"]) id: Int) {
        commentService.deleteById(id)
    }

    /**
     * Выводит список компентриев к книге
     *
     * @param bookId id книги
     */
    @ShellMethod(key = ["get-comments-by-book-id"], value = "Выводит список компентриев к книге")
    fun getCommentsByBookId(@ShellOption(value = ["--bookId"], help = "Book Id") bookId: Int): List<Comment> {
        return commentService.getByBookId(bookId)
    }

    private fun parseIdFromString(str: String?): Set<Int> {
        return str?.split(",".toRegex())
            ?.map { s -> s.toInt() }
            ?.toSet() ?: emptySet()
    }
}