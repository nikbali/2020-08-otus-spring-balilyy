package ru.otus.service.impl

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.otus.dao.CommentRepository
import ru.otus.exception.ApiException
import ru.otus.model.Comment
import ru.otus.service.BookService
import ru.otus.service.CommentService

@Service
class CommentServiceImpl(
    private val commentRepository: CommentRepository,
    private val bookService: BookService
) : CommentService {

    @Transactional
    override fun add(bookId: Int, text: String) {
        val book = bookService.getById(bookId)
        val comment = Comment()
        comment.text = text
        comment.book = book
        commentRepository.save(comment)
    }

    @Transactional
    override fun update(id: Int, text: String) {
        val comment = commentRepository.findById(id).orElseThrow { throw ApiException("Comment not found by id = $id") }
        comment.text = text
    }

    @Transactional
    override fun deleteById(id: Int) {
        commentRepository.deleteById(id)
    }

    @Transactional
    override fun getByBookId(id: Int): List<Comment> {
        return bookService.getById(id)?.comments ?: emptyList()
    }
}