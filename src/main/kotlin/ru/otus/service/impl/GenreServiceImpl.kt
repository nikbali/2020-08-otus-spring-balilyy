package ru.otus.service.impl

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.otus.dao.GenreRepository
import ru.otus.exception.ApiException
import ru.otus.model.Genre
import ru.otus.service.GenreService

@Service
class GenreServiceImpl(private val genreRepository: GenreRepository) : GenreService {

    @Transactional
    override fun create(title: String): Genre? {
        val genre = Genre()
        genre.name = title
        return genreRepository.saveAndFlush(genre)
    }

    @Transactional
    override fun update(id: Int, title: String) {
        val genre = genreRepository.findById(id).orElseThrow { throw ApiException("Genre not found by id = $id") }
        genre.name = title
    }

    @Transactional
    override fun deleteById(id: Int) {
        genreRepository.deleteById(id)
    }

    override fun getAll(): List<Genre>? {
        return genreRepository.findAll()
    }

    override fun getById(id: Int): Genre? {
        return genreRepository.findById(id)
            .orElseThrow { throw ApiException("Genre not found by id = $id") }
    }
}