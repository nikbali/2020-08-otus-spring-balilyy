package ru.otus.service.impl

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.otus.dao.AuthorRepository
import ru.otus.exception.ApiException
import ru.otus.model.Author
import ru.otus.service.AuthorService

@Service
class AuthorServiceImpl(private val authorRepository: AuthorRepository) : AuthorService {

    @Transactional
    override fun create(firstName: String, lastName: String): Author? {
        val author = Author()
        author.firstName = firstName
        author.lastName = lastName
        return authorRepository.save(author)
    }

    @Transactional
    override fun update(id: Int, firstName: String, lastName: String) {
        val author = authorRepository.findById(id).orElseThrow { throw ApiException("Author not found by id = $id") }
        author.firstName = firstName
        author.lastName = lastName
    }

    @Transactional
    override fun deleteById(id: Int) {
        authorRepository.deleteById(id)
    }

    override fun getAll(): List<Author>? {
        return authorRepository.findAll()
    }

    override fun getById(id: Int): Author? {
        return authorRepository.findById(id).orElseThrow { throw ApiException("Author not found by id = $id") }
    }
}