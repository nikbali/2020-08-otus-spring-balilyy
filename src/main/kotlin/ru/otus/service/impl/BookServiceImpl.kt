package ru.otus.service.impl

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.otus.dao.AuthorRepository
import ru.otus.dao.BookRepository
import ru.otus.dao.GenreRepository
import ru.otus.exception.ApiException
import ru.otus.model.Author
import ru.otus.model.Book
import ru.otus.model.Genre
import ru.otus.service.BookService
import java.util.stream.Collectors

@Service
class BookServiceImpl(
    private val bookRepository: BookRepository,
    private val genreRepository: GenreRepository,
    private val authorRepository: AuthorRepository
) : BookService {

    @Transactional
    override fun create(title: String, genreIds: Set<Int>, authorIds: Set<Int>): Book? {
        val book = Book()
        book.title = title
        if (genreIds.isNotEmpty()) {
            book.genres = genreRepository.findAll().stream()
                .filter { genre: Genre -> genreIds.contains(genre.id) }
                .collect(Collectors.toSet())
        }
        if (authorIds.isNotEmpty()) {
            book.authors = authorRepository.findAll().stream()
                .filter { author: Author -> authorIds.contains(author.id) }
                .collect(Collectors.toSet())
        }
        return bookRepository.save(book)
    }

    @Transactional
    override fun update(id: Int, title: String?, genreIds: Set<Int>, authorIds: Set<Int>) {

        val book = bookRepository.findById(id).orElseThrow { throw ApiException("Book not found by id = $id") }

        if (genreIds.isNotEmpty()) {
            book.genres!!.clear()
            book.genres!!.addAll(
                genreRepository.findAll().stream()
                    .filter { genre: Genre -> genreIds.contains(genre.id) }
                    .collect(Collectors.toList())
            )
        }
        if (authorIds.isNotEmpty()) {
            book.authors!!.clear()
            book.authors!!.addAll(
                authorRepository.findAll().stream()
                    .filter { author: Author -> authorIds.contains(author.id) }
                    .collect(Collectors.toList())
            )
        }
        title?.let { book.title = title }
    }

    @Transactional
    override fun deleteById(id: Int) {
        bookRepository.deleteById(id)
    }

    override fun getAll(): List<Book>? {
        return bookRepository.findAll()
    }

    override fun getById(id: Int): Book {
        return bookRepository.findById(id).orElseThrow { throw ApiException("Book not found by id = $id") }
    }
}