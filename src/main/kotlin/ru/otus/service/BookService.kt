package ru.otus.service

import ru.otus.model.Book

interface BookService {
    fun create(title: String, genreIds: Set<Int>, authorIds: Set<Int>): Book?
    fun update(id: Int, title: String?, genreIds: Set<Int>, authorIds: Set<Int>)
    fun deleteById(id: Int)
    fun getAll(): List<Book>?
    fun getById(id: Int): Book?
}