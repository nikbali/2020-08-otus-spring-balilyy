package ru.otus.service

import ru.otus.model.Comment

interface CommentService {
    fun add(bookId: Int, text: String)
    fun update(id: Int, text: String)
    fun deleteById(id: Int)
    fun getByBookId(id: Int): List<Comment>
}