package ru.otus.exception

class ApiException(message: String) : RuntimeException(message)