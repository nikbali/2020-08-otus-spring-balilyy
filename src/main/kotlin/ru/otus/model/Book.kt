package ru.otus.model

import org.hibernate.annotations.BatchSize
import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import javax.persistence.*


@Entity
@Table(name = "books")
class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null
    var title: String? = null

    @BatchSize(size = 10)
    @ManyToMany
    @Fetch(FetchMode.JOIN)
    @JoinTable(
        name = "books_to_authors",
        joinColumns = [JoinColumn(name = "book_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "author_id", referencedColumnName = "id")]
    )
    var authors: MutableSet<Author>? = null

    @BatchSize(size = 10)
    @Fetch(FetchMode.JOIN)
    @ManyToMany
    @JoinTable(
        name = "books_to_genres",
        joinColumns = [JoinColumn(name = "book_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "genre_id", referencedColumnName = "id")]
    )
    var genres: MutableSet<Genre>? = null

    @OneToMany(targetEntity = Comment::class, cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id")
    var comments: MutableList<Comment>? = null

    override fun toString(): String {
        return "Book(id=$id, title=$title, authors=$authors, genres=$genres)"
    }
}