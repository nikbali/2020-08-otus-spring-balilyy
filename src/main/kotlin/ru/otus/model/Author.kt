package ru.otus.model

import javax.persistence.*

@Entity
@Table(name = "authors")
class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    @Column(name = "first_name")
    var firstName: String? = null

    @Column(name = "last_name")
    var lastName: String? = null

    override fun toString(): String {
        return "Author(id=$id, firstName=$firstName, lastName=$lastName)"
    }
}