package ru.otus.model

import javax.persistence.*


@Entity
@Table(name = "genres")
class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null
    var name: String? = null

    override fun toString(): String {
        return "Genre(id=$id, name=$name)"
    }
}