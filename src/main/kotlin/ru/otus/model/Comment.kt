package ru.otus.model

import javax.persistence.*

@Entity
@Table(name = "comments")
class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null
    var text: String? = null

    @ManyToOne
    @JoinColumn(name = "book_id", referencedColumnName = "id")
    var book: Book? = null

    override fun toString(): String {
        return "Comment(id=$id, text=$text)"
    }
}