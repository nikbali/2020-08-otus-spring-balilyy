package ru.otus.dao

import org.springframework.data.jpa.repository.JpaRepository
import ru.otus.model.Genre

interface GenreRepository : JpaRepository<Genre, Int>