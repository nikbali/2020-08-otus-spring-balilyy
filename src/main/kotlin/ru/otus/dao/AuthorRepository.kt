package ru.otus.dao

import org.springframework.data.jpa.repository.JpaRepository
import ru.otus.model.Author

interface AuthorRepository : JpaRepository<Author, Int>
