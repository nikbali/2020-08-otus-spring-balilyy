package ru.otus.dao

import org.springframework.data.jpa.repository.JpaRepository
import ru.otus.model.Book

interface BookRepository : JpaRepository<Book, Int>
