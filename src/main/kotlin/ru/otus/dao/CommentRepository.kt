package ru.otus.dao

import org.springframework.data.jpa.repository.JpaRepository
import ru.otus.model.Comment

interface CommentRepository : JpaRepository<Comment, Int>